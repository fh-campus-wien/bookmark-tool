package ekladr.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static java.util.Collections.emptySet;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class BookmarkManagerTest {

    BookmarkManager bookmarkManager;

    static List<Bookmark> sampleBookmarks() {
        List<Bookmark> bookmarks = new ArrayList<>();
        try {
            bookmarks.add(new Bookmark(new URL("http://www.google.at"), Collections.singleton("search")));
            bookmarks.add(new Bookmark(new URL("https://www.orf.at"), Collections.singleton("info")));
            bookmarks.add(new Bookmark(new URL("http://www.test.at"), Collections.singleton("test")));
            bookmarks.add(new Bookmark(new URL("http://www.test.at/u/?f=100"), Collections.singleton("test")));
            bookmarks.add(new Bookmark(new URL("https://www.huhu.at"), Collections.singleton("info")));
            bookmarks.add(new Bookmark(new URL("http://www.xyc.at"), Collections.singleton("search")));
            bookmarks.add(new Bookmark(new URL("https://www.dada.at"), Collections.singleton("info")));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return bookmarks;
    }

    @BeforeEach
    void setUp() {
        bookmarkManager = new BookmarkManager();
    }

    /*
     * test of integrating bookmark
     */
    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5})
    void testIntegrateRatingFromBookmark(int timesBookmarkAdded) {
        Bookmark bookmark = null;
        URL urlMock = mock(URL.class);
        for (int i = 0; i < timesBookmarkAdded; i++) {
            bookmark = new Bookmark(urlMock);
            bookmarkManager.addBookmark(bookmark);
        }
        assertEquals(timesBookmarkAdded, bookmarkManager.getBookmark(bookmark.getUrl()).getRating());
    }

    @Test
    void testBookmarkNotExistOnGetRating() {
        Bookmark bm = mock(Bookmark.class);
        URL bmUrl = bm.getUrl();
        assertThrows(IllegalArgumentException.class, () -> bookmarkManager.getBookmark(bmUrl));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5})
    void testHowManyBookmarks(int numberOfBookmarks) {
        for (int i = 0; i < numberOfBookmarks; i++) {
            Bookmark bookmarkMock = mock(Bookmark.class);
            URL urlMock = mock(URL.class);
            when(bookmarkMock.getUrl()).thenReturn(urlMock);
            when(urlMock.getPath()).thenReturn(String.valueOf(numberOfBookmarks));
            when(bookmarkMock.getDomain()).thenReturn("");
            bookmarkManager.addBookmark(bookmarkMock);
        }
        assertEquals(numberOfBookmarks, bookmarkManager.getAllBookmarks().size());
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5})
    void testHowManyUrlsAreSecure(int numberOfSecureBookmarks) throws MalformedURLException {
        for (int i = 0; i < numberOfSecureBookmarks; i++) {
            /*Bookmark bookmarkMock = mock(Bookmark.class);
            URL urlMock = mock(URL.class);
            when(bookmarkMock.getUrl()).thenReturn(urlMock);
            when(urlMock.getPath()).thenReturn(String.valueOf(numberOfSecureBookmarks));
            when(bookmarkMock.isSecure()).thenReturn(true);
            bookmarkManager.addBookmark(bookmarkMock);*/
            bookmarkManager.addBookmark(new Bookmark("https://test" + i));
            bookmarkManager.addBookmark(new Bookmark("http://unsafe" + i));
        }
        assertEquals(numberOfSecureBookmarks, bookmarkManager.getSecureBookmarks().size());
    }


    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5})
    void testRemoveBookmarks(int numberOfItemsToRemove) {
        List<Bookmark> bms = sampleBookmarks();
        int bookmarkManagerInitialLoad = bms.size();

        bms.forEach(bm -> bookmarkManager.addBookmark(bm));

        bms.stream()
                .limit(numberOfItemsToRemove)
                .forEach(bm -> bookmarkManager.removeBookmark(bm.getUrl()));

        assertEquals(bookmarkManagerInitialLoad - numberOfItemsToRemove, bookmarkManager.getAllBookmarks().size());
    }

    @Test
    void testRemoveTagFromBookmark() throws MalformedURLException {
        URL bmUrl = new URL("https:://test.test/test");

        String[] tags = {"tag0", "tag1", "tag2"};

        bookmarkManager.addBookmark(new Bookmark(bmUrl, new HashSet<>(Arrays.asList(tags))));
        bookmarkManager.removeTag(bmUrl, "tag1");

        Bookmark bmk = bookmarkManager.getBookmark(bmUrl);
        assertTrue(bmk.hasTag("tag0"));
        assertFalse(bmk.hasTag("tag1"));
        assertTrue(bmk.hasTag("tag2"));
    }

    @Test
    void testDateAndTimeFromBookmark() throws MalformedURLException {

        long initialTime = System.currentTimeMillis();
        URL url = new URL("https://test.test");
        bookmarkManager.addBookmark(new Bookmark(url, emptySet()));

        long timeAtInsertion = bookmarkManager.getBookmark(url).getTimestamp().getTime();

        assertTrue(Math.abs(timeAtInsertion - initialTime) < 1000);
    }


    @ParameterizedTest
    @CsvSource({"google.com, google.com", "google.at, google.at", "orf.at, orf.at",})
    void testIsSameDomain(String url1, String url2) {
        Bookmark bookmarkMock1 = mock(Bookmark.class);
        Bookmark bookmarkMock2 = mock(Bookmark.class);
        when(bookmarkMock1.getDomain()).thenReturn(url1);
        when(bookmarkMock2.getDomain()).thenReturn(url2);
        assertTrue(bookmarkManager.isSameDomain(bookmarkMock1, bookmarkMock2));
    }

    @ParameterizedTest
    @CsvSource({"google.com, google.at", "orf.at, google.at"})
    void testIsBookmarksNotSameDomain(String url1, String url2) {
        Bookmark bookmarkMock1 = mock(Bookmark.class);
        Bookmark bookmarkMock2 = mock(Bookmark.class);
        when(bookmarkMock1.getDomain()).thenReturn(url1);
        when(bookmarkMock2.getDomain()).thenReturn(url2);
        assertFalse(bookmarkManager.isSameDomain(bookmarkMock1, bookmarkMock2));
    }

    @ParameterizedTest
    @ValueSource(strings = {"https://www.developers.google.com", "https://www.google.com/test"})
    void testBookmarkHasReference(String url) {
        Bookmark bookmark1 = null;
        Bookmark bookmark2 = null;
        try {
            bookmark1 = new Bookmark("https://www.google.com");
            bookmark2 = new Bookmark(url);
        } catch (MalformedURLException exception) {
            fail(exception.getMessage());
        }
        bookmarkManager.addBookmark(bookmark1);
        bookmarkManager.addBookmark(bookmark2);
        assertEquals(1, bookmark1.getReferences().size());
    }

    @ParameterizedTest
    @ValueSource(strings = {"https://www.developers.google.at", "https://www.orf.at"})
    void testBookmarkHasNoReference(String url) {
        Bookmark bookmark1 = null;
        Bookmark bookmark2 = null;
        try {
            bookmark1 = new Bookmark("https://www.google.com");
            bookmark2 = new Bookmark(url);
        } catch (MalformedURLException exception) {
            fail(exception.getMessage());
        }
        bookmarkManager.addBookmark(bookmark1);
        bookmarkManager.addBookmark(bookmark2);
        assertEquals(0, bookmark1.getReferences().size());
    }

    @ParameterizedTest
    @ValueSource(strings = {"https://www.developers.google.com", "https://www.google.com/test"})
    void testAddedBookmarkHasReference(String url) {
        Bookmark bookmark1 = null;
        Bookmark bookmark2 = null;
        try {
            bookmark1 = new Bookmark("https://www.google.com");
            bookmark2 = new Bookmark(url);
        } catch (MalformedURLException exception) {
            fail(exception.getMessage());
        }
        bookmarkManager.addBookmark(bookmark1);
        bookmarkManager.addBookmark(bookmark2);
        assertEquals(1, bookmark2.getReferences().size());
    }

    @ParameterizedTest
    @ValueSource(strings = {"https://www.developers.google.at", "https://www.orf.at"})
    void testAddedBookmarkHasNoReference(String url) {
        Bookmark bookmark1 = null;
        Bookmark bookmark2 = null;
        try {
            bookmark1 = new Bookmark("https://www.google.com");
            bookmark2 = new Bookmark(url);
        } catch (MalformedURLException exception) {
            fail(exception.getMessage());
        }
        bookmarkManager.addBookmark(bookmark1);
        bookmarkManager.addBookmark(bookmark2);
        assertEquals(0, bookmark2.getReferences().size());
    }

    @Test
    void shouldFindBookmarkByTag() {

        try {
            Bookmark firstBookmark = new Bookmark(new URL("http://sport.orf.at"), Collections.singleton("Sport"));
            Bookmark secondBookmark = new Bookmark(new URL("http://derstandard.at"), Collections.singleton("News"));
            Bookmark thirdBookmark = new Bookmark(new URL("http://bbc.com"), Collections.singleton("News"));
            Bookmark fourthBookmark = new Bookmark(new URL("http://nhl.com"), Collections.singleton("Sport"));
            bookmarkManager.addBookmark(firstBookmark);
            bookmarkManager.addBookmark(secondBookmark);
            bookmarkManager.addBookmark(thirdBookmark);
            bookmarkManager.addBookmark(fourthBookmark);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        assertEquals("http://nhl.com", bookmarkManager.getBookmarksByTag("Sport").stream()
                .map(bookmark -> bookmark.getUrl().toString())
                .sorted().collect(Collectors.toList()).get(0));
        assertEquals("http://sport.orf.at", bookmarkManager.getBookmarksByTag("Sport").stream()
                .map(bookmark -> bookmark.getUrl().toString())
                .sorted().collect(Collectors.toList()).get(1));
        assertEquals(2, bookmarkManager.getBookmarksByTag("News").size());
    }

    @Test
    void shouldFindBookmarkBySeveralTags() {
        try {
            Set<String> sportNewsTags = new HashSet<>();
            sportNewsTags.add("Sport");
            sportNewsTags.add("News");

            Bookmark firstBookmark = new Bookmark(new URL("http://sport.orf.at"), Collections.singleton("Sport"));
            Bookmark secondBookmark = new Bookmark(new URL("http://bbc.com"), Collections.singleton("News"));
            Bookmark thirdBookmark = new Bookmark(new URL("http://derstandard.at"), Collections.singleton("Nachrichten"));
            Bookmark fourthBookmark = new Bookmark(new URL("http://derstandard.at/sport/"), sportNewsTags);
            Bookmark fifthBookmark = new Bookmark(new URL("http://sport.at/news/"), sportNewsTags);
            bookmarkManager.addBookmark(firstBookmark);
            bookmarkManager.addBookmark(secondBookmark);
            bookmarkManager.addBookmark(thirdBookmark);
            bookmarkManager.addBookmark(fourthBookmark);
            bookmarkManager.addBookmark(fifthBookmark);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        assertEquals(
                Arrays.asList("http://derstandard.at/sport/", "http://sport.at/news/"),
                bookmarkManager.getBookmarksByTags(Arrays.asList("Sport", "News")).stream()
                        .map(bookmark -> bookmark.getUrl().toString())
                        .sorted()
                        .collect(Collectors.toList())
        );
    }

    @Test
    void testSortAllBookmarksByRatingDescending() throws MalformedURLException {
        for (String d : Arrays.asList("test.com", "orf.at", "xyz.org", "orf.at", "orf.at", "test.com")) {
            bookmarkManager.addBookmark(new URL("https://" + d + "/test"));
        }

        assertIterableEquals(
                Arrays.asList("orf.at", "test.com", "xyz.org"),
                bookmarkManager.getBookmarksSortedByRating().stream()
                        .map(b -> b.getUrl().getHost())
                        .collect(Collectors.toList())
        );
    }

    @Test
    void testSortAllBookmarksByDateNewerToOlder() throws MalformedURLException, InterruptedException {
        for (String d : Arrays.asList("test.com", "orf.at", "xyz.org", "orf.at", "abc.at", "cde.com")) {
            //Thread.sleep(5);
            await().atLeast(2, TimeUnit.MILLISECONDS).until(() -> {
                bookmarkManager.addBookmark(new URL("http://" + d));
                return true;
            });
        }

        assertIterableEquals(
                Arrays.asList("cde.com", "abc.at", "xyz.org", "orf.at", "test.com"),
                bookmarkManager.getBookmarksSortedByDate().stream()
                .map(b -> b.getUrl().getHost())
                .collect(Collectors.toList())
        );
    }
}
